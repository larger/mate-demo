Ubuntu MATE Desktop with tigervnc and xdm
=================================================

###Usage 
````
docker run -d -p 5901:5901 larger/mate-demo
````

###Account
````
 USER : admin
 PASSWD : admin
````
###Change Language 
````
 docker run -d -p 5901:5901 -e "LANG=ja_JP.UTF-8" -e "TIMEZONE=Asia/Tokyo" larger/mate-demo
````
